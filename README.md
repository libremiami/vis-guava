# vis-guava

A LibreMiami theme for the vis text editor.

![screenshot](screenshot.png)

### Installation

Install the theme from our [guix-channel](https://gitlab.com/libremiami/guix-packages) with the following command:

```sh
guix install vis-guava
```

Alternately, clone our guix-channel and build the file by invoking:

```sh
guix package -f vis-guava.scm -i vis-guava 
```

or run it in an isolated environment that includes vis: 

```sh
guix environment -l vis-guava.scm --ad-hoc vis vis-guava -- vis
```

Or clone this repo and copy guava.lua into ~/.config/vis/themes/, then type `:set theme guava`.

If you want to make the theme persistant across sessions, then put the following in ~/.config/vis/visrc.lua :

```lua
vis.events.subscribe(vis.events.INIT, function()
  vis:command('set theme guava')
  end)
  ```
